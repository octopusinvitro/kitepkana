# frozen_string_literal: true

class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.string :filename

      t.timestamps null: false
    end
  end
end
